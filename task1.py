#!/bin/usr/python3

# Looping from 2000 to 3200
for i in range(2000, 3201):
    if (i%7 == 0) and (i%5 != 0):

        #if number is divisible by 7 and not the multiple of 5, print it.
        print (str(i),',', end = "")

print('\n')
