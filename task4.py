#!/bin/usr/python3
import random
import json
import argparse

# Adding customized help to the command
parser = argparse.ArgumentParser(description = 'After the i flag, Enter the number of iterations you want the program to simulate to estimate the value of Pi')

# flag -i is for taking input for number of iterations and flag -j is for getting the iterations from a Json file
parser.add_argument("-i", type=int, help = "Takes the input from user")
parser.add_argument("-j", action = 'store_true', help='Takes the number of iterations from the Json file')

# This function does the simulation for evaluation of Pi
def pi_evaluator(iterations):
    circle_points= 0
    square_points= 0

    for _ in range(0,iterations):
        x= random.uniform(-1, 1) 
        y= random.uniform(-1, 1)
        distance =  ((x**2) + (y**2))**(1/2)

        if distance <= 1:
            circle_points += 1
        
        square_points += 1

    pi = 4* circle_points/ square_points 
    print(pi)

# getting the arguments
arg = parser.parse_args()

# if argument is -j then get the iterations from json file, else get it from user
if arg.j:
    with open('iterations.json', 'r') as file:
        json_dict = json.load(file)
        print("iteration count"," = ",json_dict['iteration_count'])  
        pi_evaluator(json_dict['iteration_count'])

elif arg.i:
    iterations = arg.i
    print("iteration count"," = ",iterations)  
    pi_evaluator(iterations)