#!/bin/usr/python3
import collections

# Takes input: the number of words
number_of_words = int(input())

# Takes inputs: actual words
list_of_words = [input() for i in range(0,number_of_words)]

# Prints number of distinct words
print(len(collections.Counter(list_of_words).keys()))

# Counts occurrences of each word
occurrences = collections.Counter(list_of_words)

for word, count in occurrences.items():
    print(count, end = " ")

