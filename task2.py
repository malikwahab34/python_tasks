#!/bin/usr/python
import argparse
parser = argparse.ArgumentParser()

# Adding parser components for user input
parser.add_argument("--UP", type=int) 
parser.add_argument("--DOWN", type=int) 
parser.add_argument("--LEFT", type=int) 
parser.add_argument("--RIGHT", type=int) 

arg = parser.parse_args()

# Computing x and y axis values
x = arg.UP - arg.DOWN
y = arg.LEFT - arg.RIGHT

# Function for computing distance
def distance_calculator(x,y):
    distance = ((x - 0)**2 + (y - 0)**2)**(1/2)
    print (round(distance))

distance_calculator(x,y)