#!/bin/usr/python3

import matplotlib.pyplot as plt

# Lists for containing the squares and cubes of the numbers provided
squared_numbers = []
cubed_numbers = []

# Functions to square and cube of the list of the numbers provided
def Square(numbers):
    global squared_numbers
    squared_numbers = [number**2 for number in numbers]
    return squared_numbers

def Cube(numbers):
    global cubed_numbers
    cubed_numbers = [number**3 for number in numbers]
    return cubed_numbers
   
numbers = [1,2,3,4,5,6,7,8,9,10]
squared_numbers = Square(numbers)
cubed_numbers = Cube(numbers)

# Plots including titles and labels
plt.figure(1)
plt.plot(numbers, squared_numbers)
plt.title("Graph of numbers vs squares")
plt.xlabel("Numbers")
plt.ylabel("Squares")

plt.figure(2)
plt.plot(numbers, cubed_numbers)
plt.title("Graph of numbers vs cubes")
plt.xlabel("Numbers")
plt.ylabel("Cubes")
plt.show()
