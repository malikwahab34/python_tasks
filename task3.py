#!/bin/usr/python3
import os

# Initializing path for checking if it exists or not
path = "/home/wahab/details"

# If path does not exist then make the directory
if (not(os.path.isdir(path))):
    os.mkdir("/home/wahab/details")

# Get the hardware details from terminal and write them on the file
with open ("/home/wahab/details/Summary.txt", 'w') as file:
    sys_details = os.popen('lscpu').read()
    file.write(sys_details)
